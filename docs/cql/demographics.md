# Demographics

## Age
Allows segmenting people based on their age

### Criterion: `Age`
Age accepts a number range, represented in bracket notation. For people under a certain age, the lower bound should be `0`.
To find people above a certain age, the upper bound should be `0`.

### Example
People who are exactly 42:
```
(Age = 42)
```
People who are above the age of 64:
```
(Age = [64, *])
```
People who are between the ages of 18 and 25:
```
(Age = [18, 25])
```

---

## Gender
Segments people by their gender.

### Criterion: `Gender`
+ `Male` - Finds men
+ `Female` - Finds women

### Example
People who are men
```
(Gender = "Male")
```

---

## Ethnicity
Segments people based on their religion.

### Criterion: `Ethnicity`
Segments people who are identified by the following ethnicity:

+ `White`
+ `Black`
+ `Asian`
+ `Hispanic`
+ `Native American`
+ `Pacific Islander`
+ `Other`

### Example
People who are Hispanic:
```
(Ethnicity = "Hispanic")
```

---

## Marital Status
Segments people based on their relationship status

### Criterion: `MaritalStatus`
+ `Single` - Finds people who are single
+ `Married` - Finds people who are married
+ `Divorced` - Finds people who are divorced
+ `Widowed` - Finds people who are widowed
+ `Separated` - Finds people who are married, but currently separated from their spouse
+ `Domesitcally Partnered` - People who are in a domestic partnership
 
### Example
People who are married:
```
(MaritalStatus = "Married")
```

---

## Children
People who have children

### Criterion: `Children`
+ `Yes` - People who have children
+ `No` - People who do not have children

### Filters
+ `ChildAge` - *Optional* - Takes in a number range that is the age of a child in the house
+ `ChildGender` - *Optional* - Takes in the same options as the top level Gender criterion, segments down on child gender

### Examples
People who do not have children:
```
(Children = "No")
```
People who has a child between the ages of 12 and 18:
```
(Children = "Yes", ChildAge = [12, 18])
```
People who have a male child:
```
(Children = "Yes", ChildGender = "Male")
```

---

## Education
People who have completed a certain level of education

### Criterion: `EducationExists`
+ `Yes` - People who have an education record
+ `No` - People who do no have an education record

### Filters
+ `EducationLevel` - *Optional* - Takes in the level of education completed 
+ `GraduationYear` - *Optional* - Takes a number range that indicates graduation years
+ `Institution` - *Optional* - Takes in the name of an institution that the education was completed at

### Example
People who have completed college:
```
(EducationExists = "Yes", EducationLevel = "Undergraduate")
```
People who have completed college after 1995:
```
(EducationExists = "Yes", EducationLevel = "Undergraduate", GraduationYear = [1995, *])
```
People who completed a graduate degree at Harvard University between the years of 1990 and 2000:
```
(EducationExists = "Yes", EducationLevel = "Graduate", GraduationYear = [1990, 2000], Institution = "Harvard University")
```
People who completed an undergraduate degree in Psychology:
```
(EducationExists = "Yes", EducationLevel = "Undergraduate", Field = "Psychology")
```

---

## Current Student
People who are currently students.

### Criterion: `CurrentStudent`
+ `Yes` - Finds people who are currently a student
+ `No` - Finds people who are currently not a student

### Example
People who are currently a student:
```
(CurrentStudent = "Yes")
```

---


## Dwelling Ownership
Segments people based on whether they own or rent their dwelling.

### Criterion: `DwellingOwnership`
+ `Own` - People who own their home
+ `Rent` - People who rent their home

### Example
People who own their dwelling:
```
(DwellingOwnership = "Own")
```

---

## Dwelling Type
Segments people based on what type of dwelling they live in.

### Criterion `DwellingType`
+ `House` - People who live in a house
+ `Apartment` - People who live in an apartment
+ `Mobile Home` - People who live in a mobile home
+ `Condo` - People who live in a condominium

### Example
People who live in an apartment:
```
(DwellingType = "Apartment")
```

---

## Dwelling Value
A number range that segments people based on the value of their dwelling

### Criterion `DwellingValue`
Takes in a number range for the value of the dwelling.

### Example
People who live in a dwelling worth more than $500,000:
```
(DwellingValue = [500000, *])
```

---

## Dwelling Years Lived
A number range that segments people based on the number of years they've lived in their dwelling

### Criterion `DwellingYearsLived`
Takes in a number range for the years lived in the dwelling.

### Example
People who've lived in a dwelling for more than 5 years:
```
(DwellingYearsLived = [5, *])
```

---

## Occupation Exists
Segmenting down on people who have occupation data

### Criterion: `OccupationExists`
+ `Yes` - People who have an occupation record
+ `No` - People who do not have an occupation record

### Filters
+ `OccupationEmployed` - *Optional* - People who are currently employed
+ `OccupationEmployer` - *Optional* - People who are employed by a specific employer
+ `OccupationTitle` - *Optional* - People who are employed with a specific title
+ `OccupationIndustry` - *Optional* - People who are employed in a specific industry
+ `OccupationSalary` - *Optional* - People who are employed earning a specific salary range
+ `OccupationStartDate` - *Optional* - People who started a job during a specific date range
+ `OccupationEndDate` - *Optional* - People who ended a job during a specific date range

### Examples
People who are currently employed as a project manager:
```
(OccupationExists = "Yes", OccupationEmployed = "Yes", OccupationTitle = "Project Manager")
```

People who are employed making over $100,000:
```
(OccupationExists = "Yes", OccupationSalary = [100000, *])
```

People who are employed by Crowdskout in Technology:
```
(OccupationExists = "Yes", OccupationEmployer = "Crowdskout", OccupationIndustry = "Technology")
```

People who started and ended a job in 2015:
```
(OccupationExists = "Yes", OccupationStartDate = [2015-01-01, 2015-12-31], OccupationEndDate = [2015-01-01, 2015-12-31])
```

---

## Business Owner
People who consider themselves business owners.

### Criterion: `BusinessOwner`
+ `Yes` - Finds people who are a business owner
+ `No` - Finds people who are not a business owner

### Example
People who consider themselves a business owner:
```
(BusinessOwner = "Yes")
```

---

## Annual Income
Allows segmenting people based on their income

### Criterion: `Income`
Income accepts a number range, represented in bracket notation. For people under a certain income, the lower bound should be `0`.
To find people above a certain income, the upper bound should be `0`.

### Example
People who make less than $25,000 a year
```
(Income = [*, 25000])
```
People who make more than $100,000:
```
(Income = [100000, *])
```
People who make between $50,000 and $100,000:
```
(Income = [50000, 100000])
```

---

## Net Worth
Allows segmenting people based on their net worth

### Criterion: `NetWorth`
Net worth accepts a number range, represented in bracket notation. For people under a certain net worth, the lower bound should be `0`.
To find people above a certain net worth, the upper bound should be `0`.

### Example
People who are worth exactly $10,000:
```
(NetWorth = 10000)
```
People who are worth more than $100,000:
```
(NetWorth = [100000, *])
```
People who are worth between $50,000 and $100,000:
```
(NetWorth = [50000, 100000])
```

---

## Head of Household
People who are the head of their household

### Criterion `HeadOfHousehold`
+ `Yes` - People who are the head of their household
+ `No` - People who are not the head of their household

### Example
People who are the head of their household:
```
(HeadOfHousehold = "Yes")
```

---

## General Interest
People who have shown interest in a specific topic.

### Criterion `Interest`
This criterion takes values dependent on the available data.

### Example
People who are interested in football:
```
(Interest = "Football")
```
People who are interested in politics:
```
(Interest = "Politics")
```

---

## Language
People who speak a specific language.

### Criterion: `Language`
Languages are populated based on available data.

### Example
People who speak Spanish:
```
(Language = "Spanish")
```

---

## Religion
Segments people by their religion.

### Criterion: `Religion`
This criterion populates based on the available data.

### Example
People who are Catholic:
```
(Religion = "Catholic")
```

---

## Veteran
People who have previously served in the armed services.

### Criterion: `Veteran`
+ `Yes` - Finds people who are a veteran
+ `No` - Finds people who are not a veteran

### Example
People who are not veterans:
```
(Veteran = "No")
```

---

## Deceased
People who have passed.

### Criterion `Deceased`
+ `Yes` - People who are deceased
+ `No` - People who are not deceased

### Filters
+ `DeceasedDate` - *Optional* - People who passed within the date range

### Examples
People who have passed:
```
(Deceased = "Yes")
```
People who passed in 2016:
```
(Deceased = "Yes", DeceasedDate = [2016-01-01, 2016-12-31])
```

---

## Investment
People who have an investment

### Criterion `Investment`
+ `Yes` - People who have an investment record
+ `No` - People who do not have an investment record

### Filters
+ `InvestmentActive` - *Optional* - People who have an active investment
+ `InvestmentAmount` - *Optional* - People who have an investment record of a specific amount
+ `InvestmentTopic` - *Optional* - People who have an investment record in a specific industry or area

### Examples
People who are active investors in "Technology":
```
(Investment = "Yes", InvestmentActive = "Yes", InvestmentTopic = "Technology")
```

People who have an old investment record in excess of $100,000:
```
(Investment = "Yes", InvestmentActive = "No", InvestmentAmount" = [100000, *])
```
