# Contact

## Address Exists
People for whom you know their address.

### Criterion `AddressExists`
+ `Yes` - People who we know their address
+ `No` - People who we do not know their address

### Filter
+ `AddressUsable` - *Optional* - People for whom their address is usable for marketing purposes.
+ `AddressLocation` - *Optional* - People for whom their address is a specific type.
+ `FullAddress` - *Optional* - People for whom they have a street address
+ `AddressCountry` - *Optional* - People who live in a specific country
+ `AddressRegion` - *Optional* - People who live in a specific region
+ `AddressCounty` - **Requires `AddressRegion` filter** - *Optional* - People who live in a specific county
+ `AddressCity` - **Requires `AddressRegion` filter** - *Optional* - People who live in a specific city
+ `AddressPostalCode` - *Optional* - People who live in a specific postal code

### Example
People whose address we know:
```
(AddressExists = "Yes")
```

People whose address we know, and we can send them direct mail:
```
(AddressExists = "Yes", AddressUsable = "Yes")
```

People whose address we know, and it is their personal address:
```
(AddressExists = "Yes", AddressLocation = "Personal")
```

People whose street address we know:
```
(AddressExists = "Yes", FullAddress = "Yes")
```

People who live in France:
```
(AddressExists = "Yes", AddressCountry = "FRA")
```

People who live in Chicago, Illinois:
```
(AddressExists = "Yes", AddressRegion = "Illinois", AddressCity = "Chicago")
```

People who live in Cook County, Illinois:
```
(AddressExists = "Yes", AddressRegion = "Illinois", AddressCounty = "Cook")
```

People who live in postal code 60637:
```
(AddressExists = "Yes", AddressPostalCode = "60637")
```

---

## Geographical Polygon
Allows you to find people within a polygon on a map.

### Criterion `Polygon`
This takes a [GeoJSON](http://geojson.org/geojson-spec.html) encoded polygon string.

### Examples
People who live within the polygon bounded by the latitude, and longitude points [0, 0], [80, 0], [80, 40], [0, 40]:
```
(Polygon = "[[[0,0], [80,0], [80,40], [0,40]]]")
```

---

## Email Exists
People for whom you know their email address.

### Criterion `EmailExists`
+ `Yes` - People who we know their email address
+ `No` - People who we do not know their email address

### Filter
+ `EmailUsable` - People for whom their email is usable for marketing purposes.
+ `EmailContact` - People for whom their email is a specific type.

### Example
People whose email address we know:
```
(EmailExists = "Yes")
```
People whose email address we know, and we can send them email:
```
(EmailExists = "Yes", EmailUsable = "Yes")
```
People whose email address we know, and it is their personal email address:
```
(EmailExists = "Yes", EmailContact = "Personal")
```

---

## Phone Exists
People for whom you know their phone number.

### Criterion `PhoneExists`
+ `Yes` - People who we know their phone number
+ `No` - People who we do not know their phone number

### Filter
+ `PhoneUsable` - People for whom their phone number is usable for marketing purposes.
+ `PhoneType` - People for whom their phone is a specific type.
+ `PhoneContact` - People for whom their phone number is a specific type.

### Example
People whose phone number we know:
```
(PhoneExists = "Yes")
```
People whose phone number we know, and we can call them:
```
(PhoneExists = "Yes", PhoneUsable = "Yes")
```
People whose phone number we know, and it is their mobile phone type:
```
(PhoneExists = "Yes", PhoneType = "Landline")
```
People whose phone number we know, and it is their professional number:
```
(PhoneExists = "Yes", PhoneContact = "Professional")
```

---

## Name Exists
People for whom you know their name.

### Criterion `NameExists`
+ `Yes` - People who we know their name
+ `No` - People who we do not know their name

### Example
People whose name we know:
```
(NameExists = "Yes")
```

---

## External ID Exists
People for whom you know an external ID.

### Criterion `ExternalIDExists`
+ `Yes` - People who we know an external ID
+ `No` - People who we do not know an external ID

### Filters
+ `ExternalIDService` - *Optional* - People who we know an external ID for in a specific service
+ `ExternalIDValue` - *Optional* - Requires **ExternalIDService** filter - People who we know a specific ID for in a specific service

### Example
People who we have a MailChimp for:
```
(ExternalIDExists = "Yes", ExternalIDService = "MailChimp")
```

People who have a MailChimp ID of abcd1234:
```
(ExternalIDExists = "Yes", ExternalIDService = "MailChimp", ExternalIDValue = "abcd1234")
```

---

## Has Payment Card
People for whom you have payment card information.

### Criterion `HasPaymentCard`
+ `Yes` - People for whom we have a payment card
+ `No` - People for whom we do not have a payment card

### Filters
+ `PaymentCardExpiration` - *Optional* - People with a payment card expiring in the specified date range
+ `PaymentCardProcessor` - *Optional* - People with a payment card processed by the specified service
+ `PaymentCardFunding` - *Optional* - People with a payment card of the specified funding type
+ `PaymentCardIssuer` - *Optional* - People with a payment card issued by the specified issuer

### Example
People with a card processed by Stripe
```
(HasPaymentCard = "Yes", PaymentCardProcessor = "Stripe")
```

People with a Visa Debit card expiring before 2020
```
(HasPaymentCard = "Yes", PaymentCardExpiration = [*, 2020-01-01], PaymentCardFunding = "Debit", PaymentCardIssuer = "Visa")
```
