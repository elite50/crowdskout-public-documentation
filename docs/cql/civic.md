# Civic

## Registered Voters
People who are registered voters.

### Criterion `RegisteredVoter`
+ `Yes` - People who are registered voters.
+ `No` - People who are not registered voters.

### Filters 
+ `RegisteredRegion` - *Optional* - People who are registered in a specific region
+ `RegisteredCounty` - *Optional* - Requires **RegisteredRegion** filter - People who are registered in a specific county
+ `RegisteredVoterStatus` - *Optional* - People who have a specific registration status ("Active", "Inactive", or "Unknown")
+ `RegisteredDate` - *Optional* - People who registered to vote in a specific date range 
+ `RegisteredPoliticalParty` - *Optional* - People who are registered with a specific political party

### Examples
People who are registered voters in Cook County, Illinois:
```
(RegisteredVoter = "Yes", RegisteredRegion = "Illinois", RegisteredCounty = "Cook")
```

People who are registered to vote and have an active status:
```
(RegisteredVoter = "Yes", RegisteredVoterStatus = "Active")
```

People who registered Democrat in 2017:
```
(RegisteredVoter = "Yes", RegisteredPoliticalParty = "Democrat", RegisteredDate = [2017-01-01, 2017-12-31])
```

---

## Registered Political Party
People who are registered to a specific political party, this information is not available in every state.

### Criterion `RegisteredParty`
This will take values dependent on your available information.

### Examples
People who registered to vote in Democratic primaries:
```
(RegisteredParty = "Democratic")
```

---

## Voting History
People who have voted in a specific election.

### Criterion `VoteHistory`
+ `Yes` - This individual has a record of voting or not
+ `No` - This individual has a record of not voting or not

### Filters
+ `Election` - *Optional* - The specific election we're looking for people to have a vote record in
+ `VoteHistoryVoted` - *Optional* - People who've specifically voted or not
+ `Early` - **Requires `VoteHistoryVoted` filter** - *Optional* - People who have voted early, when combined with `Election` looks only for people who voted early in that election
+ `Absentee` - **Requires `VoteHistoryVoted` filter** - *Optional* - People who voted absentee, when combined with `Election` looks only for people who voted absentee in that election
+ `ElectionParty` - **Requires `VoteHistoryVoted` filter** - *Optional* - People who cast a ballot for a specific party in a primary, when combined with `Election` looks for only people who voted in that election, may conflict if it's not a primary

### Example
People who've voted absentee in a primary:
```
(VoteHistory = "Yes", VoteHistoryVoted = "Yes", Absentee = "Yes")
```
People who have a vote record in the 2012 Illinois General election:
```
(VoteHistory = "Yes", Election = "2012 Illinois General")
```
People who've voted republican and early - not necessarily in the same election:
```
(VoteHistory = "Yes", VoteHistoryVoted = "Yes", ElectionParty = "Republican", Early = "Yes")
```

---

## Statewide
People who live in a specific state district.

### Criterion `Statewide`
This will take values dependent on your available information.

### Example
People who live in Michigan:
```
(Statewide = "Illinois")
```

---

## Congressional District
People who live in a specific Congressional district.

### Criterion `Congressional`
This will take values dependent on your available information and supplied filters.

### Filters
+ `Statewide` - *Required* - The state district that the city lies in.

### Example
People who live in Congressional district 09 Township in Illinois:
```
(Congressional = "09", Statewide = "Illinois")
```

---

## State Upper House
People who live in a specific state upper house district.

### Criterion `StateUpperHouse`
This will take values dependent on your available information and supplied filters.

### Filters
+ `Statewide` - *Required* - The state district that the city lies in.

### Example
People who live in the state upper house district 48 in Illinois:
```
(StateUpperHouse = "48", Statewide = "Illinois")
```

---

## State Lower House
People who live in a specific state lower house district.

### Criterion `StateLowerHouse`
This will take values dependent on your available information and supplied filters.

### Filters
+ `Statewide` - *Required* - The state district that the city lies in.

### Example
People who live in the state lower house district 062 in Illinois:
```
(StateLowerHouse = "062", Statewide = "Illinois")
```

---

## State Board of Education
People who live in a specific state board of education district.

### Criterion `StateBoardOfEducation`
This will take values dependent on your available information and supplied filters.

### Filters
+ `Statewide` - *Required* - The state district that the board of education lies in.

### Example
People who live in the state board of education district 843 in Maryland:
```
(StateBoardOfEducation = "843", Statewide = "Maryland")
```

---

## Citywide
People who live in a specific citywide district.

### Criterion `Citywide`
This will take values dependent on your available information and supplied filters.

### Filters
+ `Statewide` - *Required* - The state district that the citywide district lies in.

### Example
People who live in the Chicago, Illinois political district:
```
(Citywide = "Chicago", Statewide = "Illinois")
```

---

## City District
People who live in a specific city district.

### Criterion `CityDistrict`
This will take values dependent on your available information and supplied filters.

### Filters
+ `Statewide` - *Required* - The state district that the city district lies in
+ `Citywide` - *Required* - The citywide district that this is a district of

### Example
People who live in the "Decatur 4" city district in Decatur, Illinois:
```
(CityDistrict = "Decatur 4", Citywide = "Decatur", Statewide = "Illinois")
```

---

## City Subdistrict
People who live in a specific city subdistrict

### Criterion `CitySubdistrict`
This will take values dependent on your available information and supplied filters.

### Filters
+ `Statewide` - *Required* - The state district that this district lies in
+ `Citywide` - *Required* - The citywide district that this is a district of

### Example
People who live in the "Lincoln Land CC 526 Dist 5" city subdistrict in Lincoln Land, Illinois:
```
(CityDistrict = "Lincoln Land CC 526 Dist 5", Citywide = "Lincoln Land", Statewide = "Illinois")
```

---

## Ward
People who live in a specific ward.

### Criterion `Ward`
This will take values dependent on your available information and supplied filters.

### Filters
+ `Statewide` - *Required* - The state district that this district lies in
+ `Citywide` - *Required* - The citywide district that this is a district of

### Example
People who live in the ward 4 city subdistrict in Chicago, Illinois:
```
(Ward = "4", Citywide = "Chicago", Statewide = "Illinois")
```

---

## Countywide
People who live in a specific county.

### Criterion `Countywide`
This will take values dependent on your available information and supplied filters.

### Filters
+ `Statewide` - *Required* - The state district that the county lies in.

### Example
People who live in the Cook, Illinois political district:
```
(Countywide = "Cook", Statewide = "Illinois")
```

---

## County Commissioner District
People who live in a specific county commissioner district.

### Criterion `CountyCommissioner`
This will take values dependent on your available information and supplied filters.

### Filters
+ `Statewide` - *Required* - The state district that this district lies in.
+ `Countywide` - *Required* - The county district that the district lies in.

### Example
People who live in county commissioner district 2 in Cook County, Illinois:
```
(CountyCommissioner = "2", Countywide = "Cook", Statewide = "Illinois")
```

---

## Judicial District
People who live in a specific judicial district.

### Criterion `JudicialDistrict`
This will take values dependent on your available information and supplied filters.

### Filters
+ `Statewide` - *Required* - The state district that the city lies in.

### Example
People who live in the Judicial district 18 in Illinois:
```
(JudicialDistrict = "18", Statewide = "Illinois")
```

---

## Precinct
People who live in a certain precinct.

### Criterion `Precinct`
This will takes values dependent on your available information and supplied filters.

### Filters
+ `Statewide` - *Required* - The state district that contains the precincts we're looking for
+ `Countywide` - *Required* - The county district that contains the precincts we're looking for

### Example
People who live in precinct 7 in Cook County, Illinois:
```
(Precinct = "7", Statewide = "Illinois", Countywide = "Cook")
```

---

## School District
People who live in a specific school district

### Criterion `SchoolDistrict`
This will take values dependent on your available information and supplied filters.

### Filters
+ `Statewide` - *Required* - The state district that this district lies in.

### Example
People who live in "Scott County Schools 2" in Illinois:
```
(SchoolDistrict = "Scott County Schools 2", Statewide = "Illinois")
```

---

## High School District
People who live in a specific high school district

### Criterion `HighSchoolDistrict`
This will take values dependent on your available information and supplied filters.

### Filters
+ `Statewide` - *Required* - The state district that this district lies in.
+ `Countywide` - *Required* - The county district that the district lies in.

### Example
People who live in "Warren Township High Sch Dist 121" in Lake County, Illinois:
```
(HighSchoolDistrict = "Warren Township High Sch Dist 121", Countywide = "Lake", Statewide = "Illinois")
```

---

## Community College District
People who live in a specific community college district.

### Criterion `CommunityCollege`
This will take values dependent on your available information and supplied filters.

### Filters
+ `Statewide` - *Required* - The state district that this district lies in.

### Example
People who live in "City Colleges of Chicago 508" in Illinois:
```
(CommunityCollege = "City Colleges of Chicago 508", Statewide = "Illinois")
```

---

## Library District
People who live in a specific library district.

### Criterion `LibraryDistrict`
This will take values dependent on your available information and supplied filters.

### Filters
+ `Statewide` - *Required* - The state district that the city lies in.

### Example
People who live in the Northlake Public Library District in Illinois:
```
(LibraryDistrict = "Northlake Public Library District", Statewide = "Illinois")
```

---

## Park District
People who live in a specific park district.

### Criterion `ParkDistrict`
This will take values dependent on your available information and supplied filters.

### Filters
+ `Statewide` - *Required* - The state district that the city lies in.

### Example
People who live in Chicago Park District in Illinois:
```
(ParkDistrict = "Chicago Park District", Statewide = "Illinois")
```

---

## Township
People who live in a township.

### Criterion `Township`
This will take values dependent on your available information and supplied filters.

### Filters
+ `Statewide` - *Required* - The state district that the city lies in.

### Example
People who live in French Lick Township in Indiana:
```
(Township = "French Lick", Statewide = "Indiana")
```

---

## Village
People who live in a specific village district.

### Criterion `Village`
This will take values dependent on your available information and supplied filters.

### Filters
+ `Statewide` - *Required* - The state district that the city lies in.

### Example
People who live in the village of Gurnee, Illinois political district:
```
(Village = "Village of Gurnee", Statewide = "Illinois")
```

---

## Utility District
People who live in a specific utility district.

### Criterion `UtilityDistrict`
This will take values dependent on your available information and supplied filters.

### Filters
+ `Statewide` - *Required* - The state district that this district lies in.
+ `Countywide` - *Required* - The county district that the district lies in.

### Example
People who live in "Lost Lake Utility" in Ogle County, Illinois:
```
(UtilityDistrict = "Lost Lake Utility", Countywide = "Ogle", Statewide = "Illinois")
```

---

## Sewer District
People who live in a specific sewer district.

### Criterion `SewerDistrict`
This will take values dependent on your available information and supplied filters.

### Filters
+ `Statewide` - *Required* - The state district that this district lies in.
+ `Countywide` - *Required* - The county district that the district lies in.

### Example
People who live in "RU Twp Solid Waste Dspl" in Kane County, Illinois:
```
(SewerDistrict = "RU Twp Solid Waste Dspl", Countywide = "Kane", Statewide = "Illinois")
```

---

## Sanitation District
People who live in a specific sanitation district.

### Criterion `SanitationDistrict`
This will take values dependent on your available information and supplied filters.

### Filters
+ `Statewide` - *Required* - The state district that the city lies in.

### Example
People who live in "North Shore Sanitary District" in Illinois:
```
(SanitationDistrict = "North Shore Sanitary District", Statewide = "Illinois")
```

---

## Water District
People who live in a specific water district.

### Criterion `WaterDistrict`
This will take values dependent on your available information and supplied filters.

### Filters
+ `Statewide` - *Required* - The state district that the city lies in.

### Example
People who live in "Mahomet Aquifer Water Authority" in Illinois:
```
(WaterDistrict = "Mahomet Aquifer Water Authority", Statewide = "Illinois")
```

---

## Flood District
People who live in a specific flood district.

### Criterion `FloodDistrict`
This will take values dependent on your available information and supplied filters.

### Filters
+ `Statewide` - *Required* - The state district that this flood zone lies in.

### Example
People who live in the "7" flood district in Illinois:
```
(FloodDistrict = "7", Statewide = "Illinois")
```

---

## Watershed District
People who live in a specific watershed district.

### Criterion `WatershedDistrict`
This will take values dependent on your available information and supplied filters.

### Filters
+ `Statewide` - *Required* - The state district that this district lies in.
+ `Countywide` - *Required* - The county district that this district lies in.

### Example
People who live in "Bay Creek Watershed District" in Pike County, Illinois:
```
(WatershedDistrict = "Bay Creek Watershed District", Countywide = "Pike", Statewide = "Illinois")
```

---

## Fire District
People who live in a specific fire district

### Criterion `FireDistrict`
This will take values dependent on your available information and supplied filters.

### Filters
+ `Statewide` - *Required* - The state district that this district lies in.

### Example
People who live in "Northlake Fire Protection District" in Illinois:
```
(FireDistrict = "Northlake Fire Protection District", Statewide = "Illinois")
```

---

## Road District
People who live in a specific road district

### Criterion `RoadDistrict`
This will take values dependent on your available information and supplied filters.

### Filters
+ `Statewide` - *Required* - The state district that this district lies in.
+ `Countywide` - *Required* - The county district that the district lies in.

### Example
People who live in "Road District No. 6" in Monroe County, Illinois:
```
(RoadDistrict = "Road District No. 6", Countywide = "Monroe", Statewide = "Illinois")
```

---

## Hospital District
People who live in a specific hospital district.

### Criterion `HospitalDistrict`
This will take values dependent on your available information and supplied filters.

### Filters
+ `Statewide` - *Required* - The state district that this district lies in.
+ `Countywide` - *Required* - The county district that the district lies in.

### Example
People who live in "Illini Hospital" in Lake County, Illinois:
```
(HospitalDistrict = "Illini Hospital", Countywide = "Lake", Statewide = "Illinois")
```

---

## Census Tract
People who live in a specific Census tract.

### Criterion `CensusTract`
This will take values dependent on your available information.

### Filters
+ `Statewide` - *Required* - The state district that this district lies in.
+ `Countywide` - *Required* - The county district that the district lies in.

### Example
People who live in Census tract 1776 in Cook County, Illinois
```
(CensusTract = "1776", Statewide = "Illinois", Countywide = "Cook")
```

---

## Census Block Group
People who live in a specific census block group.

### Criterion `CensusBlockGroup`
This will take values dependent on your available information.

### Filters
+ `Statewide` - *Required* - The state district that this district lies in
+ `Countywide` - *Required* - The county district that the district lies in
+ `CensusTract` - *Required* - The census tract that the district lies in

### Example
People who live in Census block group 1700 in Census tract 1776 in Cook County, Illinois
```
(CensusBlockGroup = "1700", Statewide = "Illinois", Countywide = "Cook", CensusTract = "1776")
```

---

## Nielsen Designated Market Area
People who live in a specific Nielsen designated market area.

### Criterion `NDMA`
This will take values dependent on your available information.

### Example
People who live in the Nielsen designated market area 1776:
```
(NDMA = "1776")
```

---

## Issue Stance
People who have taken a stance on a wedge issue.

### Criterion `Issue`
This criterion takes values dependent on the available data.

### Filters
+ `Position` - *Optional* - People who have taken a specific position on the issue

### Example
People who have taken a stance on gun rights:
```
(Issue = "Gun Rights")
```
People who are pro-choice:
```
(Issue = "Abortion", Position = "Pro-choice")
```

---

## Legislation Position
People who have taken a stance on a piece of legislation.

### Criterion `LegislationPosition`
People who have taken a position on legislation

### Filters
+ `LegislationPositionDate` - *Optional* - People who took a position on legislation within a specific date range
+ `LegislationDistrict` - *Optional* - The political district where the stance was taken
+ `GovernmentalBody` - **Requires `LegislationDistrict` filter** - *Optional* - The governmental body the legislation is being considered in
+ `Legislation` - **Requires `LegislationDistrict` filter** - *Optional* - The piece of legislation under debate
+ `LegislationStance` - **Requires `Legislation` filter** - *Optional* - The stance taken on the legislation

### Examples
People who have taken a positive stance on bill 865 in Illinois State Senate:
```
(LegislationPosition = "Yes", LegislationDistrict = "Illinois", GovernmentalBody = "Illinois State Senate", Legislation = "Bill 865", LegislationStance = "Pro")
```

People who took a position on legislation in 2017:
```
(LegislationPosition = "Yes", LegislationPositionDate = [2017-01-01, 2017-12-31])
```
