# Advocacy

## Candidate Support
People who support a specific candidate in an election.

### Criterion `CandidateSupport`
This will take values dependent on your available information and list of candidates.

### Filters
+ `CandidateSupportLevel` - *Optional* - The level of support offered to the candidate
+ `CandidateSupportDate` - *Optional* - Date range when the support of the candidate was supplied

### Example
People who support Ron Paul:
```
(CandidateSupport = "Ron Paul")
```
People who are very favorable of Ron Paul:
```
(CandidateSupport = "Ron Paul", CandidateSupportLevel = "Very Favorable")
```
People who supported Ron Paul between April 1, 2014 and April 5, 2014:
```
(CandidateSupport = "Ron Paul", CandidateSupportDate = [2014-04-01,2014-04-05])
```

---

## Organization Membership
People who are a member of a specific organization.

### Criterion `Membership`
+ `Yes` - People who are members of any organization
+ `No` - People who are not members of any organization

### Filters
+ `MembershipOrganization` - *Optional* - The organization of which this profile is a member
+ `MembershipLeader` - *Optional* - Takes a yes/no depending on whether this profile is the leader of the organization
+ `MembershipActive` - *Optional* - People who are active members of an organization
+ `MembershipDonor` - *Optional* - People who are donors to the organization they're members of
+ `MembershipTitle` - *Optional* - People who have a specific title within an organization
+ `MembershipJoinDate` - *Optional* - People who joined an organization during a specific date range
+ `MembershipLeftDate` - *Optional* - People who left an organization during a specific date range

### Examples
People who belong to the AARP, but are not the leader:
```
(Membership = "Yes", MembershipOrganization = "AARP", MembershipLeader = "No")
```

People who are active donors within their organization:
```
(Membership = "Yes", MembershipActive = "Yes", MembershipDonor = "Yes")
```

People who joined an organization in 2015 and hold the title of "VIP":
```
(Membership = "Yes", MembershipTitle = "VIP", MembershipJoinDate = [2015-01-01, 2015-12-31])
```

People who left the AARP in 2000 through 2009:
```
(Membership = "Yes", MembershipOrganization = "AARP", MembershipLeftDate = [2000-01-01, 2009-12-31])
```

---

## Outside Contributions
Outside contributions - such as reported by the FEC - to an organization

### Criterion `OutsideContribution`
+ `Yes` - People who have donated at all
+ `No` - People who have no donations associated with them

### Filters
+ `OutsideContributionOrganization` - *Optional* - A filter that limits people based on the organization they have given to
+ `OutsideContributionAmount` - *Optional* - A number range that filters people based on the size of their contributions
+ `OutsideContributionTopic` - *Optional* - A filter that limits people to contributions they've made to an organization associated with a specific topic
+ `OutsideContributionPoliticalParty` - *Optional* - A filter that limits people to contributions they've made to a specific political party - only returns political parties
+ `OutsideContributionSource` - *Optional* - People who made a contribution with a specific source code
+ `OutsideContributionDatetime` - *Optional* - People who made a contribution during a provided datetime range

### Examples
People who've ever given:
```
(OutsideContribution = "Yes")
```
Outside contributions to Crowdskout over $500:
```
(OutsideContribution = "Yes", OutsideContributionOrganization = "Crowdskout", OutsideContributionAmount = [500, *])
```
Outside contributions to a republican organization:
```
(OutsideContribution = "Yes", OutsideContributionPoliticalParty = "Republican")
```
Outside contributions with the source code "I306":
```
(OutsideContribution = "Yes", OutsideContributionSource = "I306")
```

---

## Relationships
People who have a connection with another profile

### Criterion `Relationship`
Values for this criterion depend on the available information.

### Filters
+ `InfluentialRelationship` - *Optional* - Takes a yes/no depending on whether the relationship is particularly influential
+ `RelationshipType` - *Optional* - The type of relationship that this profile has with another

### Examples
People who have a relationship with "George Yates":
```
(Relationship = "George Yates")
```
People who have an influential friendship with "George Yates":
```
(Relationship = "George Yates", InfluentialRelationship = "Yes", RelationshipType = "Friend")
```
