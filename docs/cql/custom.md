# Custom Attributes

## Custom Text Attribute
The criteria that handles custom attributes holding any custom text about a user.

### Criterion `CustomText`
This will take a value that is equal to the name of a custom attribute.

### Filters
+ `CustomTextValue` - *Optional* - This is the value of the custom attribute defined above that you want to segment on. Without this, we'll just get everyone for whom the custom attribute has been applied.

### Examples
People who have been flagged as People responding to a particular email list:
```
(CustomText = "Email List Responders", CustomTextValue = "November List")
```

---

## Custom Numeric Attribute
The custom criteria that can hold a custom numeric value about a particular user. 

### Criterion `CustomNumber`
This will take a value that is equal to the name of a custom attribute.

### Filters
+ `CustomNumberValue` - *Optional* - Accepts a number range, represented in bracket notation. For people under a certain value, the lower bound should be `0`. To find people above a certain value, the upper bound should be `0`.

### Examples
People who have been marked by how many phone engagements they've had:
```
(CustomNumber = "Number of Times Called", CustomNumberValue = 15)
```
People who've been marked as being called more than twice:
```
(CustomNumber = "Number of Times Called", CustomNumberValue = [2,*])
```
People who've been marked as being called less than 5 times:
```
(CustomNumber = "Number of Times Called", CustomNumberValue = [*,5])
```

---

## Custom Radio Attribute
The custom criteria that can hold only one of several options that are available, typically from custom web forms and surveys.

### Criterion `CustomRadio`
This will take a value that is equal to the name of a custom attribute.

### Filters
+ `CustomRadioValue` - *Optional* - This is the value of the custom attribute defined above that you want to segment on. Without this, we'll just get everyone for whom the custom attribute has been applied.

### Examples
People who have agreed with a particular survey question":
```
(CustomRadio = "California Policy 555 Stance", CustomRadioValue = "Supportive")
```

---

## Custom Checkbox Attribute
The custom criteria that can hold only one of several options that are available, typically from custom web forms and surveys.

### Criterion `CustomCheckbox`
This will take a value that is equal to the name of a custom attribute.

### Filters
+ `CustomCheckboxValue` - *Optional* - This is the value of the custom attribute defined above that you want to segment on. Without this, we'll just get everyone for whom the custom attribute has been applied. Multiple values should be multiple criteria.

### Examples
People who have marked off several topics as topics they're interested in:
```
(CustomCheckbox = "Polical Policy Interests", CustomCheckboxValue = "Foreign Policy") AND
(CustomCheckbox = "Polical Policy Interests", CustomCheckboxValue = "Healthcare")
```

---

## Custom Tag Attribute
The custom criteria that can hold only one of many options that are available, from a master list of tags.

### Criterion `CustomTag`
This will take a value that is equal to the name of a custom attribute.

### Examples
People who have been tagged based on coming in from MailChimp or Adestra:
```
(CustomTag = "Importered From Mailchimp") OR 
(CustomTag = "Importered From Adestra")
```

---

## Custom Date Attribute
The custom criteria that can hold a specific date, without any time attached.

### Criterion `CustomDate`
This will take a value that is equal to the name of a custom attribute.

### Filters
+ `CustomDateRange` - *Optional* - People who are marked with this attribute in a specific date range

### Examples
People who signed up between April 15, 2014 and April 20, 2014:
```
(CustomDate = "Sign Up Date", CustomDateRange = [2014-04-15,2014-04-20])
```

---

## Custom Datetime Attribute
The custom criteria that can hold a specific date and time.

### Criterion `CustomDatetime`
This will take a value that is equal to the name of a custom attribute.

### Filters
+ `CustomDatetimeRange` - *Optional* - People who are marked with this attribute in a specific datetime range

### Examples
People who signed up between noon and 8pm on April 15, 2014:
```
(CustomDatetime = "Imported Time", CustomDatetimeRange = [2014-04-15T12:00:00+00,2014-04-15T20:00:00+00])
```