# Digital

## IP Address Exists
People for whom we have an IP address

### Criterion `IPAddressExists`
+ `Yes` - People we have an IP address for
+ `No` - People we do not have an IP address for

### Filters
+ `IPCountry` - *Optional* - People whose IP address indicates a specific country
+ `IPRegion` - **Requires `IPCountry` filter** - *Optional* - People whose IP address indicates a specific region
+ `IPCity` - **Requires `IPRegion` filter** - *Optional* - People whose IP address indicates a specific city
+ `IPCounty` - **Requires `IPRegion` filter** - *Optional* - People whose IP address indicates a specific county
+ `IPPostalCode` - **Requires `IPCountry` filter** - *Optional* People whose IP address indicates a specific postal code

### Examples
People who've digitally visited from the United States Of America:
```
(IPAddressExists = "Yes", IPCountry = "United States Of America")
```

People who've digitally visited from Chicago, Illinois:
```
(IPAddressExists = "Yes", IPCountry = "United States Of America", IPRegion = "Illinois", IPCity = "Chicago")
```

People who've digitally visited from postal code 60637:
```
(IPAddressExists = "Yes", IPCountry = "United States Of America", IPPostalCode = "60637")
```

---

## Social Media Account Exists
People who have a Social Media Account.

### Criterion `SocialMediaAccountExists`
+ `Yes` - People who we know have a Social Media Account
+ `No` - People who we do not know have a Social Media Account

### Filter
+ `SocialMediaAccountNetwork` - *Optional* - People with a social media account at a specific network
+ `SocialMediaAccountID` - **Requires `SocialMediaAccountNetwork` filter** - *Optional* - People with a social media account with a specific ID
+ `SocialMediaAccountNickname` - **Requires `SocialMediaAccountNetwork` filter** - *Optional* - People with a social media account with a specific nickname
+ `SocialMediaAccountURL` - **Requires `SocialMediaAccountNetwork` filter** - *Optional* - People with a social media account with a specific URL

### Example
People whose Facebook account we know:
```
(SocialMediaAccountExists = "Yes", SocialMediaAccountNetwork = "Facebook")
```

People with a Twitter account with the handle @georgeyatesiii:
```
(SocialMediaAccountExists = "Yes", SocialMediaAccountNetwork = "Twitter", SocialMediaAccountNickname = "georgeyatesiii")
```

People with a Facebook ID of "abcd1234" and a URL at "facebook.com/geoyates"
```
(SocialMediaAccountExists = "Yes", SocialMediaAccountNetwork = "Facebook", SocialMediaAccountID = "abcd1234", SocialMediaAccountURL = "facebook.com/geoyates")
```
