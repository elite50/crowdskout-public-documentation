# Lists

## List
Profiles who belong to a specific list.

### Criterion `CustomList`
This will take a value that is the name of the list.

### Example
People who are on the list "People to Invite":
```
(CustomList = "People to Invite")
```
