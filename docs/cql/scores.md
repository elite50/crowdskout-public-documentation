# Scores

## Engagement Score
Score representing how engaged people are in an organization

### Criterion `EngagementScore`
EngagementScore accepts a number range, represented in bracket notation.
For people under a EngagementScore, the lower bound should be 0.
To find people above a EngagementScore, the upper bound should be 0.

### Example
People who are moderately engaged in an organization:
```
(EngagementScore = [40, 70])
```

People who are very engaged in an organization:
```
(EngagementScore = [70, *]
```

---

## Political Score
Score representing people's political orientation

### Criterion `PoliticalScore`
This will take one of two strings representing the political orientation, "Republican" or "Democrat"

### Filters
+ `PoliticalScoreValue` - *Required* - Number range represented in bracket notation from 0 to 100.  Higher scores represent closer orientation with the political party.

### Example
Republican-Leaning Independent political orientation
```
(PoliticalScore = "Republican", PoliticalScoreValue = [1, 20])
```
Democrat political orientation
```
(PoliticalScore = "Democrat", PoliticalScoreValue = [20, 100])
```

---

## Custom Score Attribute
The custom criteria that can hold a custom numeric score value about a particular user. 

### Criterion `CustomScore`
This will take a value that is equal to the name of a custom attribute.

### Filters
+ `Value` - *Optional* - Accepts a number range, represented in bracket notation. For people under a certain value, the lower bound should be `0`. To find people above a certain value, the upper bound should be `0`.

### Examples
People who have been scored as having a Phone Engagement Level of 15:
```
(CustomScore = "Phone Engagement Level", Value = 15)
```
People who've been marked as having a Phone Engagement Level higher than 2:
```
(CustomScore = "Phone Engagement Level", Value = [2,*])
```
People who've been marked as having a Phone Engagement Level lower than 5:
```
(CustomScore = "Phone Engagement Level", Value = [*,5])
```
