# Search

## Search
Allows saving a search term in CQL.

### Criterion `Search`
Takes a string search term and attempts to match it against name, phone number, voter ID, email, and address.

### Examples
People whose name is George:
```
(Search = "George")
```

People whose phone number is "555.844.1313":
```
(Search = "5558441313")
```

People who live at 1336 L St SE:
```
(Search = "1336 L St SE")
```


## Profile Last Indexed
Allows getting profiles that were indexed given a date time range.

### Criterion `ProfileLastIndexed`
Takes a date time range and provides a list of profiles that were indexed within the range

### Examples
Profiles indexed after April 22nd, 12:24:23 pm with a timezone offset of -5:
```
(ProfileLastIndexed = [2017-04-22T12:24:23-05:00,*])
```