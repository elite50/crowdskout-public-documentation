# Activity

---

## Advocacy Contact
People who have contacted their legislator

### Criterion `AdvocacyContact`
+ `Yes` - People who have contacted their legislator
+ `No` - People who have not contacted their legislator

### Filters
+ `AdvocacyContactLegislator` - *Optional* - People who contacted a specific legislator
+ `AdvocacyContactMethod` - *Optional* - People who contacted their legislator using a specific contact method
+ `AdvocacyContactDatetime` - *Optional* - People who contacted their legislator during a provided datetime range
+ `AdvocacyContactPosition` - **Requires the `AdvocacyContactIssue` filter** - *Optional* - People who have a position on an issue
+ `AdvocacyContactService` - *Optional* - People who contacted their legislator through a specific service
+ `AdvocacyContactIssue` - *Optional* - People who contacted their legislator for a specific issue category
+ `AdvocacyContactEffort` - *Optional* - People who contacted their legislator as part of an effort
+ `AdvocacyContactSpecificIssue` - *Optional* - People who contacted their legislator for a specific bill or issue
+ `:Campaign` - *Optional* - People who contacted their legislator as part of a specific Crowdskout campaign
### Examples
People who contacted their legislator over email:
```
(AdvocacyContact = "Yes", AdvocacyContactMethod = "Email")
```

People who contact Mitch McConnell during February 2017:
```
(AdvocacyContact = "Yes", AdvocacyContactLegislator = "Mitch McConnell", AdvocacyContactDatetime = [2017-02-01,2017-02-28])
```
People who contact Mitch McConnell to support TPP:
```
(AdvocacyContact = "Yes", AdvocacyContactLegislator = "Mitch McConnell", AdvocacyContactIssue = "TPP", AdvocacyContactPosition = "Support")
```
People who contact Mitch McConnell as a part of the "TPP Action 2017" Crowdskout campaign
```
(AdvocacyContact = "Yes", AdvocacyContactLegislator = "Mitch McConnell", :Campaign = "TPP Action 2017")
```

---

## Conversation
People who have had a conversation started with them

### Criterion `Conversation`
+ `Yes` - People who have had a conversation started with them
+ `No` - People who have not had a conversation started with them

### Filters
+ `:Campaign` - *Optional* - The Crowdskout campaign that the conversation was initiated under
+ `ConversationEffort` - *Optional* - People who have conversation efforts under a specified conversation effort
+ `ConversationDatetime` - *Optional* - People who have conversations that were initiated during a provided datetime range
+ `ConversationOrganization` - *Optional* - People who have had conversations with a particular organization
+ `ConversationService` - *Optional* - People who have conversations that were initiated through a particular service
+ `ConversationSender` - *Optional* - Conversations started by a particular individual
+ `ConversationReceiver` - *Optional* - Conversations received by a particular individual
+ `ConversationMedium` - *Optional* - People who have conversations under a specific utm medium tag
+ `ConversationUnsubscribes` - *Optional* - Conversations that have been unsubscribed from
+ `ConversationUnsubscribeDatetime` - **Requires the `ConversationUnsubscribes` filter** - *Optional* - Conversations that have been unsubscribed to during a provided datetime range
+ `ConversationClicks` - *Optional* - People who have conversations that have been clicked
+ `ConversationClickDatetime` - **Requires the `ConversationClicks` filter** - *Optional* - Conversations that have been clicked to during a provided datetime range
+ `ConversationMessageSender` - *Optional* - Conversations sent from a specific sender
+ `ConversationMessageReceiver` - *Optional* - Conversations received by a particular person
+ `ConversationMessageReadReceipt` - *Optional* - People who have conversations that have been read
+ `ConversationReplied` - *Optional* - People who have conversations that have been replied to
+ `ConversationMessageBounceNotice` - *Optional* - People who have conversations that have bounced
+ `ConversationMessageDelivered` - *Optional* - People who have conversations that have delivered receipts
+ `ConversationMessageDatetime` - *Optional* - People who have conversations that were initiated on a specific date

### Examples
People who have conversations under a specific campaign:
```
(Conversation = "Yes", :Campaign = "Test Campaign")
```

People who have conversations that were initiated during a specified time:
```
(Conversation = "Yes", ConversationDatetime = [ 2018-10-01T10:35:19-04:00, 2018-10-21T10:35:19-04:00 ])
```

People who have conversations that they have unsubscribed from:
```
(Conversation = "Yes", ConversationUnsubscribes = "Yes")
```
People who have conversations they have replied to
```
(Conversation = "Yes", ConversationReplied = "Yes")
```

---

## Direct Mailers
Segments people who've received a direct mailer.

### Criterion `DirectMail`
+ `Yes` - People who've ever received a direct mail piece
+ `No` - People who've never received a direct mail piece

### Filters
+ `DirectMailType` - *Optional* - People who were sent a mailer of a specific type
+ `DirectMailingName` - *Optional* - People who received a specific mail piece
+ `DirectMailOrganization` - *Optional* - People who were sent a mailer from a specific Organization
+ `DirectMailingDatetime` - *Optional* - People who received a mailer during a provided datetime range
+ `:Campaign` - *Optional* - People who were sent a mailer as part of a specific Crowdskout campaign

### Example
People who received a direct mail piece from a campaign called "Mailer for Greatness":
```
(DirectMail = "Yes", DirectMailingName = "Mailer for Greatness")
```

People who were sent a postcard by Crowdskout:
```
(DirectMail = "Yes", DirectMailOrganization = "Crowdskout", DirectMailType = "Postcard")
```

---

## Donations
People who have donated to an organization

### Criterion `Donation`
+ `Yes` - People who have donated at all
+ `No` - People who have no donations associated with them

### Filters
+ `Organization` - *Optional* - A filter that limits people based on the organization they donated to
+ `Amount` - *Optional* - A number range that filters people based on the size of their donation
+ `DonationCollectionMethod` - *Optional* - A filter that limits the people based on how they made their donation
+ `DonationPaymentMethod` - *Optional* - People who made their donation with a specific payment method
+ `PoliticalParty` - *Optional* - A filter that limits people to donations they've made to a specific political party - only returns political parties
+ `DonationCampaignName` - *Optional* - People who donated to a specific donation campaign
+ `DonationRecurring` - *Optional* - People who have made a recurring donation
+ `DonationSubscriptionID` - *Optional* - People who've made a recurring donation during a specific subscription ID
+ `DonationBundlerID` - *Optional* - People who've made a donation to a specific bundler
+ `DonationExternalID` - *Optional* - People who've made a donation with a specific external ID, ex: Stripe Charge ID
+ `DonationSourceCode` - *Optional* - People who made a donation with a specific source code
+ `DonationProgram` - *Optional* - People who made a donation with a specific program
+ `DonationProgramCode` - *Optional* - People who made a donation with a specific program code
+ `DonationPackage` - *Optional* - People who made a donation with a specific package
+ `DonationPackageCode` - *Optional* - People who made a donation with a specific package code
+ `DonationFund` - *Optional* - People who made a donation with a specific fund
+ `DonationFundCode` - *Optional* - People who made a donation with a specific fund code
+ `DonationAccountCode` - *Optional* - People who made a donation with a specific account code
+ `DonationInitiativeCode` - *Optional* - People who made a donation with a specific initiative code
+ `DonationDatetime` - *Optional* - People who made their donation during a provided datetime range
+ `DonationUtmCampaign` - *Optional* - People who made their donation after arriving from a specific UTM Campaign
+ `DonationUtmMedium` - *Optional* - People who made their donation after arriving from a specific UTM Medium
+ `DonationUtmSource` - *Optional* - People who made their donation after arriving from a specific UTM Source
+ `DonationUtmContent` - *Optional* - People who made their donation after arriving from a specific UTM Content
+ `DonationUtmTerm` - *Optional* - People who made their donation after arriving from a specific UTM Term
+ `:Campaign` - *Optional* - People who made their donation as part of a specific Crowdskout campaign

### Examples
People who've ever donated:
```
(Donation = "Yes")
```
People who've made a gift to Crowdskout over $500:
```
(Donation = "Yes", Organization = "Crowdskout", Amount = [500, *])
```
People who've made an online donation to a Republican organization:
```
(Donation = "Yes", DonationCollectionMethod = "Online", PoliticalParty = "Republican")
```
People who've made a gift to Crowdskout using a check under $100 in 2012:
```
(Donation = "Yes", Organization = "Crowdskout", Amount = [*, 100], DonationDatetime = [2012-01-01, 2012-12-31], DonationPaymentMethod = "Check")
```
People who've made an online recurring donation to Crowdskout using a credit card:
```
(Donation = "Yes", Organization = "Crowdskout", Amount = [*, 100], DonationCollectionMethod = "Online", DonationRecurring = "Yes", DonationPaymentMethod = "Credit Card")
```
People who've made a donation with the source code "I306" to bundler ID "4578":
```
(Donation = "Yes", DonationSourceCode = "I306", DonationBundlerID = "4578")
```
People who've made a donation with program "HOO" to bundler ID "4578":
```
(Donation = "Yes", DonationProgram = "HOO", DonationBundlerID = "4578")
```
People who've made a donation with the program code "V540" to bundler ID "4578":
```
(Donation = "Yes", DonationProgramCode = "I306", DonationBundlerID = "4578")
```
People who've made a donation with package "Cavalier" to bundler ID "4578":
```
(Donation = "Yes", DonationPackage = "Cavalier", DonationBundlerID = "4578")
```
People who've made a donation with the package code "P434" to bundler ID "4578":
```
(Donation = "Yes", DonationPackageCode = "P434", DonationBundlerID = "4578")
```
People who've made a donation with the fund "UVA" to bundler ID "4578":
```
(Donation = "Yes", DonationFund = "UVA", DonationBundlerID = "4578")
```
People who've made a donation with the fund code "U540" to bundler ID "4578":
```
(Donation = "Yes", DonationFundCode = "U540", DonationBundlerID = "4578")
```
People who've made a donation with the account code "3333" to bundler ID "4578":
```
(Donation = "Yes", DonationAccountCode = "3333", DonationBundlerID = "4578")
```
People who've made a donation with the initiative code "I987" to bundler ID "4578":
```
(Donation = "Yes", DonationInitiativeCode = "I987", DonationBundlerID = "4578")
```
People who've made a recurring donation during the subscription identified in "abcd1234":
```
(Donation = "Yes", DonationRecurring = "Yes", DonationSubscriptionID = "abcd1234")
```
People who've made an online donation through Stripe with charge ID "abcd1234":
```
(Donation = "Yes", DonationCollectionMethod = "Online", DonationExternalID = "abcd1234")
```

---

## Email Sends
Segments people based on whether emails were sent to them.

### Criterion `EmailSend`
+ `Yes` - People who have had an email sent to them
+ `No` - People who have not had an email sent to them

### Filters
+ `Emailing` - *Optional* - People who were sent a specific emailing
+ `EmailSendOrganization` - *Optional* - People who were sent an email by a specific organization
+ `ClickedUrl` - **Requires `Emailing` filter** - *Optional* - People who clicked a URL in an email sent to them, when paired with `Emailing` it restricts to people who clicked this URL in that emailing
+ `Opened` - *Optional* - People who've opened an email sent to them, when paired with `Emailing` it restricts to having opened that emailing
+ `Clicked` - *Optional* - People who've clicked a link an email, when paired with `Emailing` it restricts to having opened that emailing
+ `Bounced` - *Optional* - People who've had an email sent to them bounce, when paired with `Emailing` it restricts to bouncing only on that emailing
+ `Unsubscribed` - *Optional* - People who unsubscribed from an emailing sent to them, when paired with `Emailing` it restricts to only people who unsubscribed after getting that emailing
+ `Complained` - *Optional* - People who marked an email sent to them as spam, when paired with `Emailing` it restricts to only people who marked that emailing as spam
+ `EmailSendService` - *Optional* - People who were sent an email through a specific service
+ `EmailSendDatetime` - *Optional* - People who were sent an email during a provide datetime range
+ `:Campaign` - *Optional* - People who were sent an email as part of a specific Crowdskout campaign

### Example
People who were sent an emailing called "March Newsletter":
```
(EmailSend = "Yes", Emailing = "March Newsletter")
```
People who've opened an emailing sent to them by Crowdskout:
```
(EmailSend = "Yes", Opened = "Yes", EmailSendOrganization = "Crowdskout")
```
People who've clicked a URL in the "March Newsletter" sent through MailChimp:
```
(EmailSend = "Yes", Emailing = "March Newsletter", Clicked = "Yes", EmailSendService = "MailChimp")
```
People who've clicked a link to "http://crowdskout.com/donate":
```
(EmailSend = "Yes", ClickedUrl = "http://crowdskout.com/donate")
```
People who've clicked a link to "http://crowdskout.com/donate" in the emailing: "March Donor Outreach":
```
(EmailSend = "Yes", Emailing = "March Donor Outreach", ClickedUrl = "http://crowdskout.com/donate")
```

---

## Independent Unsubscribes
Segments people who unsubscribed from a list separate from an email send

### Criterion `IndependentUnsubscribe`
+ `Yes` - People who have an unsubscribe
+ `No` - People who do not have an unsubscribe

### Filters
+ `IndependentUnsubscribeList` - *Optional* - People who unsubscibed from a particular list
+ `IndependentUnsubscribeContactType` - *Optional* - The type of unsubscribe (ie: SMS or Email)
+ `IndependentUnsubscribeContactValue` - *Optional* - The contact information of the entity that unsubscribed (ie: Phone number or e-mail)
+ `IndependentUnsubscribeOrganization` - *Optional* - People who have an unsubscribe from a specific organization
+ `IndependentUnsubscribeDatetime` - *Optional* - People who unsubscribed during a provide datetime range
+ `IndependentUnsubscribeContactType` - *Optional* - Type of contact to unsubscribe. Such as Email or Phone
+ `IndependentUnsubscribeContactValue` - *Optional* - Value of contact to unsubscribe. Such as the actual Email or Phone number
+ `:Campaign` - *Optional* - People who have an unsubscribe as part of a specific Crowdskout campaign

### Example
People who unsubscribed from a list called "March Newsletter":
```
(IndependentUnsubscribe = "Yes", IndependentUnsubscribeList = "March Newsletter")
```
People who have an unsubscribe connected to the Crowdskout organization:
```
(IndependentUnsubscribe = "Yes", IndependentUnsubscribeOrganization = "Crowdskout")
```
People who have unsubscribed from an email
```
(IndependentUnsubscribe = "Yes", IndependentUnsubscribeContactType = "Email")
```
People who have unsubscribed from a phone 
```
(IndependentUnsubscribe = "Yes", IndependentUnsubscribeContactType = "Phone")
```
People who have unsubscribed from a specific phone number
```
(IndependentUnsubscribe = "Yes", IndependentUnsubscribeContactValue = "1231231234")
```


---

## Event Attendance
Segments people who've attended an event

### Criterion `EventAttendance`
+ `Yes` - People who have attended an event
+ `No` - People who have not attended an event

### Filters
+ `EventAttendanceType` - *Optional* - People who attended a specific type of event
+ `EventAttendanceOrganization` - *Optional* - People who attended an event thrown by a specific organization
+ `EventAttendanceEventName` - *Optional* - The name of the event that was attended
+ `EventAttendanceHost` - *Optional* - People who attended an event hosted by a specific person or place
+ `EventAttendanceHosted` - *Optional* - People who either did or did not host an event
+ `EventAttendanceIsHost` - *Optional* - Whether or not the current organization is the event host
+ `EventAttendanceUser` - *Optional* - People whose event attendance was collected by a specific person
+ `EventAttendanceDatetime` - *Optional* - People who attended an event during a provided datetime range
+ `:Campaign` - *Optional* - People who attended an event as part of a specific Crowdskout campaign

### Examples
People who attended an event after January 1, 2016:
```
(EventAttendance = "Yes", EventAttendanceDatetime = [2016-01-01,*])
```

People who attended a House Meeting event hosted by "George Yates":
```
(EventAttendance = "Yes", EventAttendanceType = "House Meeting", EventAttendanceHost = "George Yates")
```

People who hosted a Debate Watch Party event on February 22, 2016:
```
(EventAttendance = "Yes", EventAttendanceType = "Debate Watch Party", EventAttendanceDatetime = [2016-02-22,2016-02-22], EventAttendanceHosted = "Yes")
```

People who attended an event named "Kamp Crowdskout" thrown by "Crowdskout":
```
(EventAttendance = "Yes", EventAttendanceEventName = "Kamp Crowdskout", EventAttendanceOrganization = "Crowdskout")
```

People who attended an event and had their attendance recorded by George Yates:
```
(EventAttendance = "Yes", EventAttendanceUser = "George Yates")
```

---

## Event Registrations
Segments people based on event(s) they've registered for

### Criterion `EventRegistration`
+ `Yes` - People who have registered for any event
+ `No` - People who have never registered for any event

### Filters
+ `EventName` - *Optional* - People who registered for a specific event
+ `EventRegistrationEventType` - *Optional* - People who registered for a specific type of event
+ `EventRegistrationOrganization` - *Optional* - People who registered for an event thrown by a specific organization
+ `TicketType` - **Requires the `EventName` filter** - *Optional* - People who registered/bought a specific type of ticket
+ `EventDiscountCode` - **Requires the `EventName` filter** - *Optional* - People who registered for an event using a specific code
+ `EventRegistrationEventDate` - *Optional* - People who registered for an event held during a provided date range
+ `EventRegistrationDatetime` - *Optional* - People who registered during a provided datetime range
+ `:Campaign` - *Optional* - People who registered for an event as part of a specific Crowdskout campaign

### Example
People who registered for an event called "New Years Eve Eve":
```
(EventRegistration = "Yes", EventName = "New Years Eve Eve")
```

People who registered for an event called "New Years Eve Eve" with a discount code "FREE":
```
(EventRegistration = "Yes", EventName = "New Year Eve Eve", EventDiscountCode = "FREE")
```

People who registered for a "Fundraiser" with a VIP ticket:
```
(EventRegistration = "Yes", EventRegistrationEventType = "Fundraiser", TicketType = "VIP")
```

People who registered a VIP ticket after March 20, 2015:
```
(EventRegistration = "Yes", EventRegistrationDatetime = [2015-03-20,*] TicketType = "VIP")
```

People who registered for an event held by Crowdskout in January 2016:
```
(EventRegistration = "Yes", EventRegistrationOrganization = "Crowdskout", EventRegistrationEventDate = [2016-01-01, 2016-01-31])
```

---

## Forms
Segments based on people who have filled out a specific form.

### Criterion `Form`
+ `Yes` - People who have submitted any form
+ `No` - People who have never submitted any form

### Filters
+ `FormOrganization` - *Optional* - People who submitted a form hosted by a specific organization
+ `FormType` - *Optional* - People who have submitted a specific type of form
+ `FormCollector` - *Optional* - People who have submitted a form that was collected by a specific individual
+ `FormCollectionMethod` - *Optional* - People who have submitted a form using a specific method such as in person or online
+ `FormName` - *Optional* - People who submitted a specific form
+ `FormQuestion` **Requires the `FormName` filter be present** - *Optional* - People who have answered a specific question in that form
+ `FormAnswer` - **Requires the `FormQuestion` filter be present** - *Optional* - People who have given a specific answer in the form
+ `FormDatetime` - *Optional* - People who filled out the form during a provided datetime range
+ `:Campaign` - *Optional* - People who submitted a form as part of a specific Crowdskout campaign

### Example
People who filled out a form called "Crowdskout Newsletter Sign Up":
```
(Form = "Yes", FormName = "Crowdskout Newsletter Sign Up")
```

People who filled out a form hosted by Crowdskout:
```
(Form = "Yes", FormOrganization = "Crowdskout")
```

People who filled out a form called "Crowdskout Newsletter Sign Up" online before 2015:
```
(Form = "Yes", FormName = "Crowdskout Newsletter Sign Up", FormCollectionMethod = "Online", FormDatetime = [*,2014-12-31])
```

People who gave the answer "Yes" to the question "Do you like adventures?" in the "Crowdskout Newsletter Sign Up" form:
```
(Form = "Yes", FormName = "Crowdskout Newsletter Sign Up", FormQuestion = "Do you like adventures?", FormAnswer = "Yes")
```

People who answered a survey in person from George Yates:
```
(Form = "Yes", FormType = "Survey", FormCollector = "George Yates", FormCollectionMethod = "In Person")
```

---

## Knock
Segments based on people who have had their door knocked.

### Criterion `Knock`
+ `Yes` - People who have ever had their door knocked by you.
+ `No` - People who have not had their door knocked by you.

### Filters
+ `KnockOrganization` - *Optional* - People who had a knock from a specific organization
+ `DayTripName` - *Optional* - People who had their door knocked during a specific DayTrip
+ `Route` - **Requires the `DayTripName` filter** - *Optional* - People who have had their door knocked on a specific route during a specific DayTrip
+ `KnockResult` - *Optional* People who had a specific result to their door being knocked, takes the values: `Not Home`, `Refused`, and `Survey Ran`
+ `KnockVolunteer` - *Optional* - People who had their door knocked by a specific volunteer
+ `SurveyName` - *Optional* - People who had a specific survey filled out when their door was knocked
+ `KnockDatetime` - *Optional* - People who had their door knocked during a provided datetime range
+ `:Campaign` - *Optional* - People who had a knock as part of a specific Crowdskout campaign

### Example
People who had their door knocked during the "Chicago Canvassing" DayTrip run by Crowdskout:
```
(Knock = "Yes", Name = "Chicago Canvassing", KnockOrganization = "Crowdskout")
```
People whose doors were knocked on, but they were not home in January 2015:
```
(Knock = "Yes", KnockResult = "Not Home", KnockDatetime = [2015-01-01,2015-01-31])
```
People whose doors were knocked on for survey "Food Survey" by volunteer "Fred Knox":
```
(Knock = "Yes", SurveyName = "Food Survey", KnockVolunteer = "Fred Knox")
```
---

## Meeting
People who have a face to face meeting tracked in Crowdskout.

### Criterion `Meeting`
Takes a "Yes" value for people who have had a meeting.

### Filters
+ `MeetingOrganization` - *Optional* - People who had a meeting with a member of a specific organization
+ `MeetingUser` - *Optional* - People who had a meeting with a specific user
+ `MeetingType` - *Optional* - People who had a specific type of meeting
+ `MeetingPurpose` - *Optional* - People who had a meeting with a specific purpose
+ `MeetingLocation` - *Optional* - People who had a meeting in a specific location
+ `MeetingDatetime` - *Optional* - People who had a meeting during a specific datetime range
+ `:Campaign` - *Optional* - People who had a meeting as part of a specific Crowdskout campaign

### Examples
People who have had a face-to-face meeting with someone from Crowdskout:
```
(Meeting = "Yes", MeetingOrganization = "Crowdskout")
```

People who have had a general face to face meeting with George Yates after March 2nd, 2016:
```
(Meeting = "Yes", MeetingUser = "George Yates", MeetingDatetime = [2016-03-02,*])
```

People who had a meeting to discuss volunteering in the office:
```
(Meeting = "Yes", MeetingPurpose = "Volunteering", MeetingLocation = "Office")
```

---

## Note
People who have a note written about them inside Crowdskout

### Criterion `Note`
Takes a "Yes" value for people who have a note about them

### Filters
+ `NoteAuthor` - *Optional* - People who had a note written about them by a specific author
+ `NoteDatetime` - *Optional* - People who had a note written about them during a specific datetime range

### Examples
People who have a note about them written by George Yates:
```
(Note = "Yes", NoteAuthor = "George Yates")
```

People who had a note written about them in 2016:
```
(Note = "Yes", NoteDatetime = [2016-01-01, 2016-12-31])
```

---

## Page Views
People who have a page view attached to them

### Criterion `PageView`
+ `Yes` - People who have viewed your web page
+ `No` - People who have not view your web page

### Filters
+ `PageViewOrganization` - *Optional* - People who have viewed a web page hosted by a specific organization
+ `Url` - *Optional* - Only people who've seen a specific web page, URLs will be stripped of
+ `PageViewUtm` - *Optional* - Yes/No - Only people who've viewed a web page with any utm tag
+ `PageViewUtmSource` - **Requires the PageViewUtm filter** - *Optional* - Only people who've viewed a web page with a specific utm source tag
+ `PageViewUtmMedium` - **Requires the PageViewUtm filter** - *Optional* - Only people who've viewed a web page with a specific utm medium tag
+ `PageViewUtmCampaign` - **Requires the PageViewUtm filter** - *Optional* - Only people who've viewed a web page with a specific utm campaign tag
+ `PageViewUtmTerm` - **Requires the PageViewUtm filter** - *Optional* - Only people who've viewed a web page with a specific utm term tag
+ `PageViewUtmContent` - **Requires the PageViewUtm filter** - *Optional* - Only people who've viewed a web page with a specific utm content tag
+ `Platform` - *Optional* - Only people who've viewed a web page on a specific browsing platform
+ `PageViewDatetime` - *Optional* - People who've viewed your website during a provide datetime range
+ `:Campaign` - *Optional* - People who viewed a web page as part of a specific Crowdskout campaign

### Examples
People who have ever visited Crowdskout's web page:
```
(PageView = "Yes", PageViewOrganization = "Crowdskout")
```

People who have visited your web site on a mobile device before January 3, 2015:
```
(PageView = "Yes", Platform = "Mobile", PageViewDatetime = [*,2015-01-03])
```

People who visited "http://crowdskout.com" sometime between November 1, 2014 and November 31, 2014:
```
(PageView = "Yes", Url = "http://crowdskout.com", PageViewDatetime = [2014-11-01, 2014-11-31])
```

People who visited a web page with the a utm_source tag of "email":
```
(PageView = "Yes", PageViewUtm = "Yes", PageViewUtmSource = "email")
```

---

## Social Share
People who have performed a social share

### Criterion `SocialShare`
+ `Yes` - People who have shared something to social platforms
+ `No` - People who have not shared something to social platforms

### Filters
+ `SocialShareUrl` - *Optional* - People who have shared this specific URL to social platforms
+ `ShareService` - *Optional* - People who have shared to a specific service
+ `SocialShareDatetime` - *Optional* - People who've shared something during a provided datetime range
+ `:Campaign` - *Optional* - People who shared something as part of a specific Crowdskout campaign

### Examples
People who have ever shared crowdskout's webpage to a social service:
```
(SocialShare = "Yes", SocialShareUrl = "Crowdskout.com")
```

People who have shared something to Facebook before January 3, 2015:
```
(SocialShare = "Yes", ShareService = "Facebook", SocialShareDatetime = [*,2015-01-03])
```

---

## Social Engage
People who have engaged with a social platform

### Criterion `SocialEngage`
+ `Yes` - People who have engaged with a social platform
+ `No` - People who have not engaged with a social platform

### Filters
+ `SocialEngageEngages` - *Optional* - People who have engaged with a social service in a specific way
+ `EngageService` - *Optional* - People who have engaged with a specific service
+ `SocialEngageDatetime` - *Optional* - People who've engaged with a social platform during a provided datetime range
+ `:Campaign` - *Optional* - People who engaged with something as part of a specific Crowdskout campaign

People who have engaged with something on Facebook before January 3, 2015:
```
(SocialEngage = "Yes", EngageService = "Facebook", SocialEngageDatetime = [*,2015-01-03])
```

---

## Video Plays
Segments people based on embedded videos they've watched

### Criterion `VideoPlay`
+ `Yes` - People who have watched any video
+ `No` - People who have never watched any video

### Filters
+ `VideoPlayOrganization` - *Optional* - People who watched a video created or distributed by a specific organization
+ `VideoService` - *Optional* - People who watched a video hosted by a specific service
+ `VideoTitle` - *Optional* - People who watched a specific video
+ `VideoURL` - *Optional* - People who watched a video at a specific URL
+ `VideoFinished` - *Optional* - People who finished a video, combines with VideoTitle to segment people who finished a specific video
+ `VideoPlayDatetime` - *Optional* - People who watched a video during a provided datetime range
+ `:Campaign` - *Optional* - People who watched a video as part of a specific Crowdskout campaign

### Example
People who watched a video entitled "Ducks on Roombas" at "https://youtube.com/abcd1234":
```
(VideoPlay = "Yes", VideoTitle = "Ducks on Roombas", VideoURL = "https://youtube.com/abcd1234")
```

People who started, but did not finish a video entitled "Ducks on Roombas" before August 15, 2014:
```
(VideoPlay = "Yes", VideoTitle = "Ducks on Roombas", VideoPlayDatetime = [*,2014-08-15], VideoFinished = "No")
```

People who watched a video embedded from YouTube distributed by Crowdskout:
```
(VideoPlay = "Yes", VideoService = "YouTube", VideoPlayOrganization = "Crowdskout")
```

---

## Phone Calls
Segments people who've received a phone call.

### Criterion `PhoneCall`
+ `Yes` - People who have ever received a call
+ `No` - People who have never received a call

### Filters
+ `PhoneCallOrganization` - *Optional* - People who received a call from an organization
+ `PhoneCallType` - *Optional* - People who received a call of a specific type
+ `PhoneCallName` - *Optional* - People who received a specific phone call
+ `CallResult` - *Optional* - People who picked up, did not answer, etc. When paired with `PhoneCallName` it segments only people who had the result in that campaign
+ `CallLength` - *Optional* - People who received a call of a specific duration
+ `PhoneCallUser` - *Optional* - People who made the phone call
+ `PhoneCallDatetime` - *Optional* - People who received a phone call during a provided datetime range
+ `:Campaign` - *Optional* - People who received a call as part of a specific Crowdskout campaign

### Example
People who received a robocall for a campaign called "Robocall for Greatness":
```
(PhoneCall = "Yes", PhoneCallName = "Robocall for Greatness", PhoneCallType = "Robocall")
```

People who answered a phone call July 15, 2015 or earlier:
```
(PhoneCall = "Yes", CallResult = "Picked Up", PhoneCallDatetime = [*,2015-07-15])
```

People who received a phone call from John Smith July 15, 2015 or later:
```
(PhoneCall = "Yes", PhoneCallUser = "John Smith", PhoneCallDatetime = [2015-07-15,*])
```

People who received a call from Crowdskout that lasted over a minute:
```
(PhoneCall = "Yes", PhoneCallOrganization = "Crowdskout", CallLength = [60, *])
```

---

## Third Party Submissions
Segments people who filled out a non-Crowdskout form on a web page

### Criterion `ThirdPartySubmission`
+ `Yes` - People who have filled out a non-Crowdskout form
+ `No` - People who have never filled out a non-Crowdskout form

### Filters
+ `ThirdPartySubmissionOrganization` - *Optional* - People who filled out a non-Crowdskout form hosted by a specific organization
+ `ThirdPartySubmissionUrl` - *Optional* - People who filled out a non-Crowdskout form on a specific URL
+ `ThirdPartySubmissionDatetime` - *Optional* - People who filled out a non-Crowdskout form during a provided datetime range
+ `:Campaign` - *Optional* - People who filled out a non-Crowdskout form as part of a specific Crowdskout campaign

### Examples
People who filled out a form on http://google.com hosted by Google:
```
(ThirdPartySubmission = "Yes", ThirdPartySubmissionUrl = "http://google.com", ThirdPartySubmissionOrganization = "Google")
```

People who filled out a form June 1, 2015 or later:
```
(ThirdPartySubmission = "Yes", ThirdPartySubmissionDatetime = [2015-06-01,*])
```

---

## Voter Registration Interaction
Segments people who've registered to vote with the client

### Criterion `VoterRegistrationInteraction`
+ `Yes` - People who have a voter registration interaction
+ `No` - People who do not have a voter registration interaction

### Filters
+ `VoterRegistrationOrganization` - *Optional* - The organization this voter registration was collected for
+ `VoterRegistrationType` - *Optional* - People who filled out a specific type of voter registration
+ `VoterRegistrationParty` - *Optional* - People who registered for a specific political party
+ `VoterRegistrationCollector` - *Optional* - People who registered with a specific individual
+ `VoterRegistrationLead` - *Optional* - The person who lead the collection effort, may not be the individual who registered voter
+ `VoterRegistrationRole` - *Optional* - The role of the individual collecting the registration
+ `VoterRegistrationLocation` - *Optional* - The location where the registration was collected
+ `VoterRegistrationLocationType` - *Optional* - The location type where the registration was collected
+ `VoterRegistrationPetitionType` - *Optional* - The type of petition that was signed when the individual was registered to vote
+ `VoterRegistrationDatetime` - *Optional* - People who registered to vote during a provided datetime range
+ `:Campaign` - *Optional* - People who registered to vote as part of a specific Crowdskout campaign

### Example
People who registered in 2016 for "GoTV":
```
(VoterRegistrationInteraction = "Yes", VoterRegistrationDatetime = [2016-01-01,2016-12-31], VoterRegistrationOrganization = "GoTV")
```

People who filled out a "Change of Address" registration, and registered for the Republican party:
```
(VoterRegistrationInteraction = "Yes", VoterRegistrationType = "Change of Address", VoterRegistrationParty = "Republican")
```

People who had their registration collected by an RLI Staffer, "James Keary" under the direction or "George Yates":
```
(VoterRegistrationInteraction = "Yes", VoterRegistrationCollector = "James Keary", VoterRegistrationLead = "George Yates", VoterRegistrationRole = "RLI Staffer")
```

People who registered to vote after signing a School Choice petition at an event:
```
(VoterRegistrationInteraction = "Yes", VoterRegistrationPetitionType = "School Choice", VoterRegistrationLocationType = "Event")
```

People who registered to vote at PS 138:
```
(VoterRegistrationInteraction = "Yes", VoterRegistrationLocation = "PS 138")
```
