# Campaigns

---

## Campaigns
People who are a part of a Crowdskout campaign

### Criterion `Profile`
+ `Yes` - Matches all profiles

### Filters
+ `:Campaign` - *Optional* - People who are a part of a specific campaign

### Examples

People belonging to a 2018 Fundraising campaign

```
(Profile = "Yes", :Campaign = "2018 Fundraising Campaign")
```
