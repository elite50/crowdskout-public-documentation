# Developing Crowdskout Email Templates

## Overview

Transforming a normal email template into a Crowdskout email template is simple.
We've extended the HTML vocabulary, giving you the ability to add functionality to sections of
your template. The ability to add editable areas or parse client specific variables are all
handled internally, so you don't have to worry about it. Simply code the email html and css.
Then, add our Crowdskout specific html attributes and variables into your template and
we'll handle the rest.

Crowdskout specific attributes are applied the same way as any standard HTML attribute, 
such as `id` or `class`. This allows us to add custom functionality on top of your template.
All Crowdskout specific attributes are prefixed with `cs-`. For example, to make a section in
your template editable, you would add a `cs-editable` attribute to the wrapping html element.
You can find a list of all usable attributes in our
[Attribute Reference](email-templates/attribute-reference/)

We also provide variables to be used throughout your template. These variables are specific
to each client using your template. So, when a client changes their logo or address, there's
no need to edit the email template every time. All Crowdskout variables are prefix with `cs::`.
For example, if you wanted to display the company's name in your template, you would use
`cs::organization-name`. You can find a list of all usable variables in our
[Variable Reference](email-templates/variable-reference/)

## HTML &amp; CSS Structure

The biggest hassle when coding an email template is inlining your css. This means appending each
individual style to a specific html element's style attribute. With Crowdskout templates,
there's no need to inline your styles. We take care of that for you. 

What about responsive meta tags or proper doctypes? You don't have to worry about that either.
You only need to code the body of the email.

## Images

All email images must be hosted somewhere. We provide two options for images. Our clients 
can provide a URL of an image that they are hosting on their own servers. Or, they can upload
an image using our email editor, which we will then host for them. You can find a further
description of this under the [`cs-placeholder` documentation](email-templates/attribute-reference/#cs-placeholder)

As for the client's logo image, we allow them to upload an image in their organization's settings
page. The URL of this image is available as a Crowdskout variable in your template.

Except for the logo, any images that are hard coded into the template, must be hosted by the client.
Crowdskout does not provide an image hosting service for all images.