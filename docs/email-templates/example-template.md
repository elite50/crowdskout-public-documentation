# Example Template
Below is a fully-functioning Crowdskout email template that conforms to proper
e-mail HTML standards and uses most of the Crowdskout specific styles outlined earlier.

```
<style>
  body {
    font-family: 'Helvetica Neue';
    margin:0;
    padding:0;
  }
</style>
<!--Full width -->
<table width="100%">
  <tr>
    <td align="center">
      <table width="500px">
        <tr>
          <!-- Logo area -->
          <td>
            <a href="cs::organization-url">
              <img cs-src="cs::organization-logo" alt="cs::organization-name" />
            </a>
          </td>
          
          <!-- Content area -->
          <td cs-editable cs-format="bold,italic,underline,justifyLeft,justifyCenter,justifyRight">
            This text in here will be editable!
            
            <!-- Image placeholder -->
            <img cs-placeholder>
          </td>
          
          <!-- Action area -->
          <td cs-editable cs-format="bold,italic">
            <a href="" cs-button>
              Editable button text
            </a>
          </td>
        </tr>
      </table>
    </td>
  <tr>
<table>
```