# Variable Reference

All variables are client specific. Each client has an organization settings page where these variables are updated.

## cs::organization-name
Prints the organization name.

### Example
`Crowdskout`

## cs::organization-logo
Prints the URL of the organization's logo.

### Example
`http://crowdskout.com/image.png`

## cs::organization-url
Prints the URL of the organization's website.

### Example
`http://crowdskout.com`

## cs::organization-address
Prints the street, city, state, and zip code of the organization's address.

### Example
`1875 Connecticut Ave Washington, DC 20037`

## cs::primary-color
Prints the clients primary color stored in their organization settings page.

### Example
`#00aeef`

## cs::secondary-color
Prints the client's secondary color stored in their organization settings page.

### Example
`#666666`

## cs::current-year
Prints the current year. 

### Example
`2015`