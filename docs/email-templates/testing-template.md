# Testing a Template
To test a template, you will need a Crowdskout account. Follow the below steps to view your email or send a
test email to view it in an inbox.

## Steps
1. Head over to [https://app.crowdskout.com](https://app.crowdskout.com). If you are not logged in,
it should redirect you to the login page. 
1. Once you're logged in, click on "Actions" in the sidebar.
1. Click on the "Email" icon. In the top right, there will be a "New Email" button to click.
1. A new modal should pop up. Make sure the "Crowdskout" tab is selected, not the "External Service" tab.
Fill out the form (the values don't matter). Click on "Create".
1. Fill out the "Setup" step and then click "Templates" on the bottom right corner of the page.
1. Once you're on the templates page, click the "Custom" tab. This will give you the option to upload
your own custom template.
1. Click on "Upload". Provide a Template Name and select your template to upload. It should be a .html file.
1. Click on your newly updated template to select it, and click on the "Customize" button in the bottom right.
1. This will bring up the email creation view using your template. From here, you can send a test email to yourself
or your boss to view the design in an actual inbox.
1. To refresh, go through steps 1-5 again, delete the template you just uploaded, and upload a new one.